package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_authentication.*


class AuthenticationActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        auth = FirebaseAuth.getInstance()
        init()
    }
    private fun init() {
        logInButton.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
        signUpButton.setOnClickListener {
            val intent = Intent( this, SignUpActivity::class.java)
            startActivity(intent)
        }

        fun signIn(){
            val email:String = emailBox.text.toString()
            val password = passwordBox.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty()){
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            d("signIn", "signInWithEmail:success")
                            val user = auth.currentUser
                            Toast.makeText(this, "Success",Toast.LENGTH_SHORT).show()
                        } else {
                            w("signIn", "signInWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "User Authentication Failed.", Toast.LENGTH_LONG).show()
                        }
                    }
            }else{Toast.makeText(this,"Please fill all fields",Toast.LENGTH_SHORT).show()}
        }
    }
}
