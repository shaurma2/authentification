package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.regex.Pattern
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init() {
        auth = Firebase.auth
        signup2.setOnClickListener {
            signup()
        }


    }
    private fun  signup(){
        val mail:String = signuptext.text.toString()
        val password:String  = passwordsignup.text.toString()
        val passwordconf:String = passwordconfirmation.text.toString()
        if (mail.isNotEmpty() && password.isNotEmpty() && passwordconf.isNotEmpty()){
            if (password==passwordconf){
                auth.createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            d("signup", "createUserWithEmail:success")
                            val user = auth.currentUser
                        } else {
                            w("signup", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }else {
                Toast.makeText(this, "Passwords Do not Match", Toast.LENGTH_SHORT).show()

            }}else{
            Toast.makeText(this, "Please Fill ALL Fields", Toast.LENGTH_SHORT).show()
        }
    }

}


